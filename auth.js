///auth.js - Authentication module
//Handles registering, logging in and checking sessionID's

var bcrypt = require('bcryptjs');
var express = require('express');
var router = express.Router();


//Export module as router
module.exports = router;
