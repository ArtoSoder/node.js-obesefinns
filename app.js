
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var favicon = require('serve-favicon');
var app = express();
var bodyParser = require('body-parser');
var serverjs = require('./servermainjs.js');
//var sqlite = require('sqlite3').verbose();
var fs = require('fs');

var auth = require('./auth.js');

/*
var dbFile = "obesefinnsdatabase.db";
var db = new sqlite.Database(dbFile, function(err)
	{
		if(err)
		{
			console.log("error opening database");
		}
	});

//var exists = fs.existsSync(dbFile);
db.serialize(function()
{
	db.run("CREATE TABLE if not exists contacts (name TEXT, email TEXT, message TEXT)");

});

//db.close();

*/

// all environments

app.set('port', process.env.PORT || 80);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname, 'public/img/', 'favicon.ico')));
app.use(bodyParser.json());

//Redirect all requests from /auth to auth-module
app.use('/auth', auth);

app.use(bodyParser.urlencoded({
 	extended: true
 }));


app.get('/obesefinns', routes.obesefinns);
app.get('/', function(req, res)
{
	res.redirect('/obesefinns');

});

app.get('/obesefinns/admin',routes.admin);

app.post('/', function(req,res)
{
	var email = req.body.email;
	var name = req.body.name;
	var message = req.body.message;

	if(email && name && message)
	{
		var data = {
			email: email,
			name: name,
			message: message
		};

		if(serverjs.validateFormInfo(data))
		{
			/*
			db.serialize(function()
			{
				db.run("INSERT INTO contacts VALUES (?,?,?)",name,email,message);
			});

			//db.close();
			*/
			res.send("OK");		
		}
		else { res.send("NOT OK"); }
	}
	else
	{
		res.sendStatus("NOT OK");
	}
});


// Create server and listen to port 3000
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
