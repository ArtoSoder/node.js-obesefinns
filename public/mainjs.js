function sendFormData()
{

	//CHECK THAT EMAIL HAS @ AND .  + NAME IS SET AND MESSAGE IS SET
	var validEmail = false;

	var email = $('#contact-email').val();
	var name = $('#contact-name').val();
	var message = $('#contact-message').val();

	if(email && name && message && message.length >= 10 && name.length >= 2)
	{
		var reg = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
		if(reg.test(email))
		{
			console.log("Email is good");
			validEmail = true;
		}
	}
	else if(!message || message.length < 10)
	{
		contactFormResponse("Message must be at least 10 chars long.", "error");
		return;
	}
	else if(!email || !validEmail)
	{
		contactFormResponse("Not a valid email.", "error");
		return;
	}
	else if(!name || name.length < 2)
	{
		contactFormResponse("Insert name that's at least 2 chars long.", "error");
		return;
	}

	var data = {
		email: email,
		name: name,
		message: message	
	};

	$.post("/", data, function(res)
	{
		if(res === "OK")
		{
			contactFormResponse("Thank you for contacting us!", "info");
			//$('#contact-email').val("");			
			//$('#contact-name').val("");
			//$('#contact-message').val("");
			//$('#send-form').prop("disabled", true);


			//Hide the contact panels and buttons
			$('#contact-email').slideUp('fast');
			$('#contact-name').slideUp('fast');
			$('#contact-message').slideUp('fast');
			$('#send-form').slideUp('fast');


		}
		else if(res === "NOT OK")
		{
			contactFormResponse("Error posting data. Name must be at least 2 chars long, message at least 10 chars and a valid email must be inserted.", "error");
		}
	});

}

function contactFormResponse(message, type)
{
	$('#contact-guide').empty();

	if(type === "error")
		$('#contact-guide').append("<p class='error-message'>" +message + "</p>");
	else if(type === "info")
		$('#contact-guide').append("<p class='info-message'>" + message + "</p>");
}

$(document).ready(function(){


//var $playingSound = false;

	$("img.teamimage").hover(function()
	{

		/*
		if(!$playingSound)
		{
			$playingSound = true;
			var $rand = Math.floor(Math.random() * 31 + 1);
			console.log($rand);

			$.ionSound.play("fat_bastard_" + $rand);
			console.log("fat_bastard_" + $rand);

		}
	*/

		//$(this).addClass("img-responsive");
		
		//$(this).removeClass("img-circle");
		$(this).addClass("img-thumbnail");
		//$(this).removeClass("teamimage");
		$(this).addClass("teamimage-zoom");
	},
		function() {
			//$(this).removeClass("img-responsive");
			//$(this).addClass("img-circle");
			$(this).removeClass("img-thumbnail");
			//$(this).addClass("teamimage");
			$(this).removeClass("teamimage-zoom");
		}
	);
/*
	$.ionSound({
		sounds: [
			{
				name:"fat_bastard_1"
			},
						{
				name:"fat_bastard_2"
			},
						{
				name:"fat_bastard_3"
			},
						{
				name:"fat_bastard_4"
			},
						{
				name:"fat_bastard_5"
			},
						{
				name:"fat_bastard_6"
			},
						{
				name:"fat_bastard_7"
			},
						{
				name:"fat_bastard_8"
			},
						{
				name:"fat_bastard_9"
			},
						{
				name:"fat_bastard_10"
			},
						{
				name:"fat_bastard_11"
			},
						{
				name:"fat_bastard_12"
			},
						{
				name:"fat_bastard_13"
			},
						{
				name:"fat_bastard_14"
			},
						{
				name:"fat_bastard_15"
			},
						{
				name:"fat_bastard_16"
			},
						{
				name:"fat_bastard_17"
			},
						{
				name:"fat_bastard_18"
			},
						{
				name:"fat_bastard_19"
			},
						{
				name:"fat_bastard_20"
			},
						{
				name:"fat_bastard_21"
			},
						{
				name:"fat_bastard_22"
			},
						{
				name:"fat_bastard_23"
			},
						{
				name:"fat_bastard_24"
			},
						{
				name:"fat_bastard_25"
			},
			{
				name:"fat_bastard_26"
			},
						{
				name:"fat_bastard_27"
			},
						{
				name:"fat_bastard_28"
			},
						{
				name:"fat_bastard_29"
			},
									{
				name:"fat_bastard_30"
			},
									{
				name:"fat_bastard_31"
			},

		],

		volume:1,
		path: "sounds/",
		preload:false,
		multiplay: false,
		ended_callback: function(obj)
		{
			
			$playingSound = false;
		}

	});

*/

});