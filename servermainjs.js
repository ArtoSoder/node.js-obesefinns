exports.validateFormInfo = function(data)
{
		var email = data.email;
		var message = data.message;
		var name = data.name;

		if(email && message && name && message.length >= 10 && name.length >= 2)
		{
			var reg = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
			if(reg.test(email))
			{
				return true;
			}
			else
				return false;
		}
		else
		{
			return false;
		}

};